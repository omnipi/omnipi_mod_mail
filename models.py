from django.db import models

# Create your models here.
class Tag(models.Model):
	tag_value = models.TextField()


class Email(models.Model):

	mssg_number = models.TextField()			# assigned by the mailbox
	mssg_id = models.TextField(primary_key=True, unique=True) 	# as comes from the message

	from_addr = models.TextField()				# the value of the FROM header
	from_name = models.TextField(default="")	# parsed out 'name' from the FROM header

	to_addr = models.TextField()
	to_name = models.TextField(default="")

	cc_addr = models.TextField(default="")

	subject = models.TextField()
	body = models.TextField(blank=True)
	email_date = models.DateTimeField()

	thread_topic = models.TextField(blank=True, default="-")
	thread_index = models.TextField(blank=True, default="-")
	
	attachments = models.TextField(default="")

	# provide the tagging relationship
	tags = models.ManyToManyField(Tag, blank=True)

	# flag for synchronizing db and file
	sync_flag = models.IntegerField(default=0)