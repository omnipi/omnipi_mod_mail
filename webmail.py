from django.utils import timezone
from django.conf import settings

import os
import email.utils
import email.parser
from email.message import Message
import mailbox
import time
import datetime
import random

# on *nix based machines, add in gunicorn
if os.name == 'posix':
    import chardet

from mod_mail.models import Email

import omnipi.timezone as optz
import poplib
# import opproxy.popsocks as popsocks

# remove mail from the server
deleteFromServer = True

# attachment root folder
attachmentRoot = os.path.join(settings.OP_ROOT, "store", "_files", "mail")

# mail root folder
mailboxRoot = os.path.join(settings.OP_ROOT, "store", "mail")
mailBox = mailbox.Maildir(mailboxRoot)



# TODO: Handle the 'Sender' header, which records the TRUE sender, used on behalf of 'From'
# TODO: Handle the 'In-Reply-To' header for conversations too.
# TODO: Return threads, rather than individual messages. Use subject similarity and thread info



def getAccounts():
    return ['omnipi']


def sync(account, mailBox):
    pop = poplib.POP3('mail.example.com')
    pop.user('omnipi@example.com')
    pop.pass_('omnipi')

    new_messages = 0
    old_messages = 0

    num_messages = len(pop.list()[1])
    print('Total messages:', num_messages)
    if num_messages > 25:
        num_messages = 25

    for i in range(num_messages):
        print("\nProcessing message", i)

        # process the downloaded mail; fetch and decode the message
        newmessage = not processmail(pop.retr(i + 1)[1])

        if newmessage:
            new_messages += 1
        else:
            old_messages += 1

        # mark the email for deletion
        if deleteFromServer:
            print("##### removing from POP server")
            pop.dele(i + 1)

    # finally, hang up the connection, which will commit all deletes
    pop.quit()

    return new_messages, old_messages


def processmail(mssg_text):
    parser = email.parser.Parser()
    message = parser.parsestr("\n".join(mssg_text))
    mailexisted = True

    # get the message from the db
    mssg_id = message.get('message-id')
    mail = Email.objects.filter(mssg_id=mssg_id)

    # if the mail is not in the db, we process it
    if mail.count() == 0:

        # mail_record = unpackMessage(message, 0)
        message_dict = {}
        mail_record = transcodeMessage(message, 0, message_dict)

        # TODO: the DB save and the mailbox save should be atomic...

        # save the message to the db
        e, created = Email.objects.get_or_create(mssg_id=mssg_id, defaults=mail_record)
        e.save()

        # if the db added it, then add it to the mailbox
        if created:
            print("##### adding to maildir")
            mssg_number = mailBox.add(message)
            # add the message number from the mailbox to the record
            Email.objects.filter(mssg_id=mssg_id).update(mssg_number=mssg_number)
            mailexisted = False

    return mailexisted


def transcodeMessage(mssg, id, saveMail):
    # save from/to/cc/subject/date/
    if id == 0:

        for h in mssg.items():

            # save the mail for the db too
            if h[0].lower() == 'from':
                addr = email.utils.parseaddr(h[1])
                if addr[0] == "":
                    saveMail['from_addr'] = h[1]
                else:
                    saveMail['from_name'] = addr[0]
                    saveMail['from_addr'] = addr[1]

            # get some basic email data for quick display
            if h[0].lower() == 'to': saveMail['to_addr'] = h[1]
            if h[0].lower() == 'cc': saveMail['cc_addr'] = h[1]
            if h[0].lower() == 'subject': saveMail['subject'] = h[1]
            if h[0].lower() == 'message-id': saveMail['mssg_id'] = h[1]

            # get some thread information
            if h[0].lower() == 'thread-topic': saveMail['thread_topic'] = h[1]
            if h[0].lower() == 'thread-index': saveMail['thread_index'] = h[1]

            # try and parse the date and if we have problems, use 'now'
            if h[0].lower() == 'date':
                bestGuess = email.utils.parsedate_tz(h[1]) # returns a struct_time
                if bestGuess != None:
                    print("######### using the parsed date")
                    mailTime = email.utils.mktime_tz(bestGuess) # returns a float
                    saveMail['email_date'] = datetime.datetime.fromtimestamp(mailTime, optz.opTimezone(
                        bestGuess[9])) #email.utils.parsedate(h[1]) #time.mktime(bestGuess)
                else:
                    print("######### using the now date")
                    saveMail['email_date'] = timezone.now()
                print(saveMail['email_date'])

    # a multipart message needs to be further reduced
    if mssg.is_multipart():
        print("###### FOUND MULTIPART")
        payload = mssg.get_payload()
        for m in payload:
            transcodeMessage(m, id + 1, saveMail)

    # the core message is not multipart, or we are parsing a subpart
    # decode if not text/plain
    else:
        payload = mssg.get_payload(decode=True)
        contentType = mssg.get_content_type()
        contentCharset = mssg.get_content_charset('ascii')

        if contentType == 'text/html':
            print("### GOT HTML", contentCharset)
            try:
                payload = payload.decode(contentCharset).encode('utf-8')
            except:
                print("Unable to decode according to specified encoding")
                if contentCharset == 'us-ascii':
                    print("Attempting to detect encoding")
                    predicted = chardet.detect(payload)
                    try:
                        payload = payload.decode(predicted['encoding']).encode('utf-8')
                    except:
                        print("Failed using explicit/default encoding and detected encoding. Leaving alone")
            saveMail['body'] = payload
        # saveMail['html_body'] = payload

        elif contentType == 'text/plain':
            print("### GOT PLAIN TEXT", contentCharset)
            payload = payload.decode(contentCharset).encode('utf-8')
            saveMail['body'] = payload
        # saveMail['text_body'] = payload

        else:
            print("### ATTACHMENT")
            friendlyMssgIdPath = saveMail['mssg_id'].replace("<", "").replace(">", "")
            attachmentPath = os.path.join(attachmentRoot, friendlyMssgIdPath)
            # filenames can be in either the content-disposition (preferred) or the content-type headers
            poss_fn_header = mssg.get("content-disposition")
            if poss_fn_header == None:
                poss_fn_header = mssg.get("content-type")
            if poss_fn_header != None:
                filename = parseContentName(poss_fn_header)
            else:
                filename = "OMNIPI_ATTACH_" + randomFileName()
            try:
                os.makedirs(attachmentPath)
                attach = open(os.path.join(attachmentPath, filename), 'wb')
                attach.write(payload)
            except:
                pass

    return saveMail


def parseContentName(dispos):

    filename = None;
    dispoParams = dispos.split(";")
    for param in dispoParams[1:]:
        print("DISPOS:", param)
        name, value = param.split("=")
        if name.strip() == 'filename':
            filename = value.strip().replace('"', '')
    return filename


def randomFileName(len=8):
    possible_chars = string.ascii_uppercase + string.digits
    return ''.join(random.choice(possible_chars) for i in range(len))


def syncMailWithDb(mailBox):
    # get all the items in the mailbox
    print("syncing mailbox with db")

    allEmail = mailBox.items()
    for mssg in allEmail:

        mssgId = mssg[1]['message-id']
        mssgNum = mssg[0]

        # get the message from the db
        mail = Email.objects.filter(mssg_id=mssgId)
        mailExists = mail.count()

        # see what the value of mssg_number is, this should be the mailbox id
        if mailExists:
            for m in mail:
                Email.objects.filter(mssg_id=mssgId).update(mssg_number=mssgNum)
                print("Updated message id: ", mssgId, "with the message number", mssgNum)

        else:
            print("Can't find email with id:", mssgId)

        # print mssg[0], mssg[1]['message-id'], mailExists
