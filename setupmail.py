from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render, render_to_response, redirect
from django.conf import settings
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt

from mod_mail.models import Email
from mod_mail.settings import MailSettings


def settings(request, **args):
    if request.method == "POST":
        settings_form = MailSettings(request.POST)
        if settings_form.is_valid():
            return HttpResponseRedirect('./thanks/')

    else:
        settings_form = MailSettings()

    return render(request, 'omnipi/settings.html', {'form': settings_form})

    # context = {}
    # context['AppName'] = "WebMail"
    # context["appname"] = "mail"
    # context["pagename"] = "Settings"
    # context['settings'] = [
    #     {
    #         "title": "mail_server",
    #         "name": "Mail Server",
    #         "type": "text",
    #         "value": "pop3.example.com",
    #         "validation": "url",
    #     }, {
    #         "title": "protocol_type",
    #         "name": "Mail Protocol",
    #         "type": "radio",
    #         "value": ["POP3", "IMAP"],
    #     }, {
    #         "title": "username",
    #         "name": "User Name",
    #         "type": "text",
    #         "value": "omnipi@example.com",
    #     }, {
    #         "title": "password",
    #         "name": "Password",
    #         "type": "password",
    #         "value": "0mniP1",
    #     }, {
    #         "title": "mail_signature",
    #         "name": "Signature",
    #         "type": "textarea",
    #         "value": "Sent from OmniPi - http://omnipi.org/",
    #     },
    # ]
    # return render(request, 'omnipi/settings.html', context)

def gotosetup():
    return HttpResponseRedirect('./setup/')

def thanks(request):
    context = {}
    context['body'] = '''<p>Thank you for setting up, we'll see how we get on with those!</p>

    <p><a href="../">Back to OnmiPi Mail</a></p>'''
    return render(request, 'omnipi/just_body.html', context)    

def setup(request):
    context = {}
    context['body'] = '''<p>It looks like OmniPi Mail is not currently setup. To configure mail, please follow the
    setup link and enter the appropriate mail settings for your mail server. Currenlty, OmniPi Mail does not
    support SMTP itself, so you will need to provide the address of both an incoming POP3 or IMAP server as well
    as an SMTP server for your outgoing mail.</p>

    <p><a href="./settings">Setup you OnmiPi Mail</a></p>'''
    return render(request, 'omnipi/just_body.html', context)    