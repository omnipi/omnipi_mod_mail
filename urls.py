from django.conf.urls import patterns, url

from mod_mail import views, setupmail

urlpatterns = patterns('',
	# ex: /polls/
	url(r'^$', views.index, name='index'),

	# fetch an email data
	url(r'^email', views.fetchHtml, name='fetchHtml'),
	url(r'^fetch', views.fetch, name='fetch'),

	# submit an email for sending
	url(r'^send', views.send, name='send'),

	# maintenance urls
	url(r'^refresh', views.refresh, name='refresh'),
	url(r'^sync', views.sync, name="sync"),

	# settings and setup
	url(r'^setup', setupmail.setup, name="setup"),
	url(r'^settings', setupmail.settings, name='settings'),
	url(r'^thanks', setupmail.thanks, name='thanks'),

)